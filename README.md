# OpenML dataset: Meta_Album_TEX_DTD_Micro

https://www.openml.org/d/44250

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Textures-DTD Dataset (Micro)**
***
The Textures DTD dataset(https://www.robots.ox.ac.uk/~vgg/data/dtd/index.html) is a large textures dataset which consists of 5 640 images. The data is collected from Google and Flicker by the original authors of the paper 'Describing Textures in the Wild'. The data was annotated using Amazon Mechanical Turk. The data collection process is mentioned on the dataset overview page. For Meta-Album meta-dataset, this dataset is preprocessed by cropping the images to square images and then resizing them to 128x128 using Open-CV with an anti-aliasing filter. This dataset has 47 class labels.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/TEX_DTD.png)

**Meta Album ID**: MNF.TEX_DTD  
**Meta Album URL**: [https://meta-album.github.io/datasets/TEX_DTD.html](https://meta-album.github.io/datasets/TEX_DTD.html)  
**Domain ID**: MNF  
**Domain Name**: Manufacturing  
**Dataset ID**: TEX_DTD  
**Dataset Name**: Textures-DTD  
**Short Description**: Textures dataset from Describable Textures Dataset  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: textures, manufacturing  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Open for research purposes  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: Describable Textures Dataset (DTD), University of Oxford, England  
**Source URL**: https://www.robots.ox.ac.uk/~vgg/data/dtd/  
  
**Original Author**: Mircea Cimpoi, Subhransu354Maji, Iasonas Kokkinos, Sammy Mohamed, Andrea Vedaldi  
**Original contact**: {mircea, vedaldi}@robots.ox.ac.uk  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@InProceedings{cimpoi14describing,
	Author    = {M. Cimpoi and S. Maji and I. Kokkinos and S. Mohamed and and A. Vedaldi},
	Title     = {Describing Textures in the Wild},
	Booktitle = {Proceedings of the {IEEE} Conf. on Computer Vision and Pattern Recognition ({CVPR})},
	Year      = {2014}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44294)  [[Extended]](https://www.openml.org/d/44328)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44250) of an [OpenML dataset](https://www.openml.org/d/44250). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44250/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44250/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44250/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

